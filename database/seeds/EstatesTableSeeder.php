<?php

use Illuminate\Database\Seeder;
use App\Models\Estate;

class EstatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        Estate::truncate();

        Estate::create([ 'name' => 'The Victoria', 'price' => 374662, 'bedrooms_count' => 4, 'bathrooms_count' => 2, 'storeys_count' => 2, 'garages_count' => 2 ]);
        Estate::create([ 'name' => 'The Xavier', 'price' => 513268, 'bedrooms_count' => 4, 'bathrooms_count' => 2, 'storeys_count' => 1, 'garages_count' => 2 ]);
        Estate::create([ 'name' => 'The Como', 'price' => 454990, 'bedrooms_count' => 4, 'bathrooms_count' => 3, 'storeys_count' => 2, 'garages_count' => 3 ]);
        Estate::create([ 'name' => 'The Aspen', 'price' => 384356, 'bedrooms_count' => 4, 'bathrooms_count' => 2, 'storeys_count' => 2, 'garages_count' => 2 ]);
        Estate::create([ 'name' => 'The Lucretia', 'price' => 572002, 'bedrooms_count' => 4, 'bathrooms_count' => 3, 'storeys_count' => 2, 'garages_count' => 2 ]);
        Estate::create([ 'name' => 'The Toorak', 'price' => 521951, 'bedrooms_count' => 5, 'bathrooms_count' => 2, 'storeys_count' => 1, 'garages_count' => 2 ]);
        Estate::create([ 'name' => 'The Skyscape', 'price' => 263604, 'bedrooms_count' => 3, 'bathrooms_count' => 2, 'storeys_count' => 2, 'garages_count' => 2 ]);
        Estate::create([ 'name' => 'The Clifton', 'price' => 386103, 'bedrooms_count' => 3, 'bathrooms_count' => 2, 'storeys_count' => 1, 'garages_count' => 1 ]);
        Estate::create([ 'name' => 'The Geneva', 'price' => 390600, 'bedrooms_count' => 4, 'bathrooms_count' => 3, 'storeys_count' => 2, 'garages_count' => 2 ]);
    }
}
