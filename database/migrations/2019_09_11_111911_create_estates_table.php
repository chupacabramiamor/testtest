<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->tinyInteger('bedrooms_count')->default(0)->unsigned();
            $table->tinyInteger('bathrooms_count')->default(0)->unsigned();
            $table->tinyInteger('storeys_count')->default(0)->unsigned();
            $table->tinyInteger('garages_count')->default(0)->unsigned();
            $table->decimal('price', 16, 2)->default(0)->unsigned();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
}
