window.jQuery = window.$ = require('jquery');
window.Vue = require('vue');

var VueResource = require('vue-resource').default;
var BlockUIMixin = require('./mixins/blockui.js');

require('blockui-npm');

Vue.config.devtools = true;
Vue.use(VueResource);

var FilterFormComponent = require('./components/FilterForm/component.vue').default;
var ResultComponent = require('./components/Result/component.vue').default;
var BlockUIDirective = require('./directives/blockui.js');

Vue.component('filter-form', FilterFormComponent);
Vue.component('result', ResultComponent);
Vue.directive('blockui', BlockUIDirective);

var preloader = (new Image()).src = '/images/preloader.svg';

const app = new Vue({
    el: '#vue-app',

    mixins: [ BlockUIMixin ],

    mounted: function() {
        this.$on('filterUpdated', this.lockAppHandler);
        this.$on('dataReceived', this.unlockAppHandler);
    },

    methods: {
        lockAppHandler: function() {
            this.setPreloader('app');
        },

        unlockAppHandler: function() {
            this.unsetPreloader('app');
        }
    }
});
