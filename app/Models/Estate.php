<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estate extends Model
{
    public $timestamps = false;

    public function scopeByKeyword($query, string $value)
    {
        return $query->where('name', 'like', sprintf('%%%s%%', $value));
    }

    public function scopeByPriceRange($query, float $from = 0, float $to = 0)
    {
        if ($to > 0) {
            return $query->whereBetween('price', [ $from, $to ]);
        }

        return $query->where('price', '>=', $from);
    }
}
