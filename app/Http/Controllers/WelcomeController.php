<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estate;

class WelcomeController extends Controller
{

    public function getIndex()
    {
        return view('welcome');
    }

    public function getData(Request $request)
    {
        $builder = Estate::query();

        // Filter by searching Keyword
        if ($request->has('keyword')) {
            $builder = $builder->byKeyword($request->input('keyword'));
        }

        // Filter by bedrooms count
        if ($request->has('bedrooms_count')) {
            $builder = $builder->whereBedroomsCount($request->input('bedrooms_count'));
        }

        // Filter by bathrooms count
        if ($request->has('bathrooms_count')) {
            $builder = $builder->whereBathroomsCount($request->input('bathrooms_count'));
        }

        // Filter by storeys count
        if ($request->has('storeys_count')) {
            $builder = $builder->whereStoreysCount($request->input('storeys_count'));
        }

        // Filter by garages count
        if ($request->has('garages_count')) {
            $builder = $builder->whereGaragesCount($request->input('garages_count'));
        }

        // Filter by price range
        $builder = $builder->byPriceRange($request->input('price_from', 0), $request->input('price_to', 0));

        return $builder->get();
    }
}
